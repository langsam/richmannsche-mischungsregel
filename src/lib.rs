extern crate wasm_bindgen;

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn get_m1(t1: f32, t2: f32, tm: f32, m2: f32) -> f32 {
    ((t2 * m2) - (tm * m2)) / (tm - t1)
}
