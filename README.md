# Richmannsche Mischungsregel

Build a Webassembly module that provides functions that allow different calculations based on the [Richmannsche Mischungsregel](https://de.wikipedia.org/wiki/Richmannsche_Mischungsregel).

## Build

To build the npm package:

```bash
wasm-pack build --scope [npm-username]
```

To publish the npm package:

```bash
cd pkg
npm publish --access=public
```